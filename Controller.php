<?php

class Controller {
    public $player1;
    public $player2;
    public $id_game;
    public $step_game;
            
    function authentication()
    {
        if(isset($_POST['login']))
        {
            
            return $_SESSION['player1'] = MainMenu::login();
        }elseif (isset ($_POST['registr'])) {
            MainMenu::registrs();
            return $_POST['logs'];
        }
    }
    
    function startGames()
    {
        $playGame = new Engine();
        
        $save = [
            'x'=> $_SESSION['id'], 'o'=>NULL, 't'=>0,
            'm'=>'0='.implode(',', $playGame->map), 's'=>0];
        $this->id_game = Store::savenGameDB($save);
        
        View::viewPlayer($this->id_game, 0,$this->player1, 'себя');
        View::viewMap($playGame->map);
    }
    
    function renewalGame()
    {
        $playGame = new Engine();
        
        $id = htmlspecialchars($_GET["id"]);
        
        $data = Store::loadGameDB($id);
        
        $playGame->map = $this->definitionMap($data['MAP'], $data['STEP']);
        $this->step_game = $data['STEP'] + 1;
        $playGame->clikButton = $this->clickButton();
        $playGame->turn = $data['TURN'];
            
        if(!$playGame -> progressGames())
        {
            if(!$playGame->turn)
            {
                return View::viewWinPlayer("твой воображаемы друг");
            }
            View::viewWinPlayer($this->player1);
            
        }else{
            $save = [
                't'=>$playGame->turn,
                'm'=>$data['MAP']."\n"
                    .$this->step_game.'='
                    .implode(',', $playGame->map),
                's'=>$this->step_game];

            $this->id_game = Store::savenGameDB($save, $id);

            View::viewPlayer(
                    $id, $this->step_game,
                    $this->player1, 'себя');
            View::viewMap($playGame->map);            
        }

    }
    
    protected function definitionMap($data, $step)
    {
        if(htmlspecialchars($_GET["step"]) != NULL)
        {
            $step = htmlspecialchars($_GET["step"]);
        }
        $stepMap = explode("\n", $data);
        foreach ($stepMap as $value)
        {
            $map = explode("=", $value);
            if($step == $map[0])
            {
                //var_dump(explode(",", $map[1]));
                return explode(",", $map[1]);
            }
        }
    }

        protected function clickButton()
    {

        for ($i = 1; $i <= 9; $i++)
        {
            if($_POST[$i])
            {
                return $i;
            }
        }
    }
}
