<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of engine
 *
 * @author alexey
 */
class Engine {
    public $map = [1,2,3,4,5,6,7,8,9];
    public $turn = TRUE;
    public $clikButton;
    
    public function progressGames()
    {
        $turn = $this->turn;
        if($turn)
        {
            $turn = FALSE;
        }else{
            $turn = TRUE;
        }
        $this->turn = $turn;
        $this->map = $this->updateMap();
        if($this->checkwin())
        {
            //var_dump($this->checkwin());
            return FALSE;
        }
        return TRUE;
    }
    

    
    protected function updateMap()
    {
    
        if($this->turn)
        {
            $this->map[$this->clikButton-1] = 'X';
        }else{
            $this->map[$this->clikButton-1] = 'O';
        }
        return $this->map;
    }
    
    function checkwin()
    {
        $map = $this->map;
        if($this->turn)
        {
            $t = "X";
        }else{
            $t = "O";
        }
        if (
            # horizontal
            ($map[0] == $t && $map[1] == $t && $map[2] == $t) || 
            ($map[3] == $t && $map[4] == $t && $map[5] == $t) || 
            ($map[6] == $t && $map[7] == $t && $map[8] == $t) || 
            # vertical
            ($map[0] == $t && $map[3] == $t && $map[6] == $t) || 
            ($map[1] == $t && $map[4] == $t && $map[7] == $t) ||
            ($map[2] == $t && $map[5] == $t && $map[8] == $t) ||
            # diagonal
            ($map[0] == $t && $map[4] == $t && $map[8] == $t) ||
            ($map[2] == $t && $map[4] == $t && $map[6] == $t))
        {  
        return TRUE;
        }
        return FALSE;
    }
}
