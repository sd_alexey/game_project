<?php 
session_start();
include_once 'Controller.php';
include_once 'MainMenu.php';
include_once 'games/Store.php';
include_once 'games/view.php';
include_once 'games/engine.php';

?>
<html>
  <head>
    <meta charset="utf-8">
    <link href="style.css" rel="stylesheet"> 
    <title>Добро пожаловать на Test.ru!</title>
  </head>
  <body> 

<?php
if(isset($_POST['exit'])){
    $_SESSION = NULL;
    $_POST = NULL;
    $_GET = NULL;
    session_destroy();
}

$game = new Controller();
$game->id_game = $id = htmlspecialchars($_GET["id"]);
$game->step_game = htmlspecialchars($_GET["step"]);
$game->player1 = $_SESSION['player1'];


if(!$_SESSION['player1']){
    View::presentLogin();
    $game->player1 = $game->authentication();
}

if(isset($_POST['start']))
{
    $game->startGames();
}elseif($id != FALSE and !isset($_POST['menu'])){
    $game->renewalGame();
}elseif ($game->player1 or isset($_POST['menu'])) {
    $_GET = NULL;
    View::presentMenuUser($game->player1);
    MainMenu::editTableStoreg();
}

?>
  </body>
</html>